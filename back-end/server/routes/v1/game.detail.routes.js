const GameDetailController = require('../../controllers/game.detail.controller');
const gameDetailRouter = require('express').Router();

/**
 * @Routes "/api/v1/Users"
 */

gameDetailRouter.get('/', GameDetailController.getGameDetail);
gameDetailRouter.post('/', GameDetailController.createGameDetail);
gameDetailRouter.get('/:id', GameDetailController.getGameDetailById);
gameDetailRouter.put('/:id', GameDetailController.updateGameDetail);
gameDetailRouter.delete('/:id', GameDetailController.deleteGameDetail);

module.exports = gameDetailRouter;
